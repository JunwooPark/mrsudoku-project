(ns mrsudoku.model.solver
  (:require
            [mrsudoku.model.grid :as g]
            [mrsudoku.model.conflict :as c]))

(defn solve
  "Solve the sudoku `grid` by returing a full solved grid,
 or `nil` if the solver fails."
  [grid]
  (loop [grid grid, cx 1, cy 1, val 1, count 1, rev 1, prev_collision 100]
    (if (= :init (get (g/cell grid cx cy) :status))
      (if (= rev 1)
        (if (and (= cx 9) (= cy 9))
          grid
          (if (= cx 10)
            nil
            (if (= cy 9)
              (recur grid (inc cx) 1 1 1 rev prev_collision)
              (recur grid cx (inc cy) 1 1 rev prev_collision))))
        (if (= cy 1)
          (if (= (get (g/cell grid (dec cx) 9) :value) 9)
            (recur grid (dec cx) 9 (inc (get (g/cell grid (dec cx) 9) :value)) 0 rev prev_collision)
            (recur grid (dec cx) 9 (inc (get (g/cell grid (dec cx) 9) :value)) 1 rev prev_collision))
          (if (= (get (g/cell grid cx (dec cy)) :value) 9)
            (recur grid cx (dec cy) (inc (get (g/cell grid cx (dec cy)) :value)) 0 rev prev_collision)
            (recur grid cx (dec cy) (inc (get (g/cell grid cx (dec cy)) :value)) 1 rev prev_collision))))
      (let [grid (g/change-cell grid cx cy (g/mk-cell :set val))]
        (println "conflicts = ",(c/grid-conflicts grid))
        (if (or (= count 0) (not= (c/grid-conflicts grid) {}))
          (if (>= val 9)
            (if (= cy 1)
              (if (= (get (g/cell grid (dec cx) 9) :value) 9)
                (recur (g/change-cell grid cx cy (g/mk-cell :set prev_collision)) (dec cx) 9 (inc (get (g/cell grid (dec cx) 9) :value)) 0 -1 (inc prev_collision))
                (recur (g/change-cell grid cx cy (g/mk-cell :set prev_collision)) (dec cx) 9 (inc (get (g/cell grid (dec cx) 9) :value)) 1 -1 (inc prev_collision)))
              (if (= (get (g/cell grid cx (dec cy)) :value) 9)
                (recur (g/change-cell grid cx cy (g/mk-cell :set prev_collision)) cx (dec cy) (inc (get (g/cell grid cx (dec cy)) :value)) 0 -1 (inc prev_collision))
                (recur (g/change-cell grid cx cy (g/mk-cell :set prev_collision)) cx (dec cy) (inc (get (g/cell grid cx (dec cy)) :value)) 1 -1 (inc prev_collision))))
            (recur grid cx cy (inc val) 1 1 prev_collision))
          (if (and (= cx 9) (= cy 9))
            grid
            (if (= cx 10)
              nil
              (if (= cy 9)
                (if (= :empty (get (g/cell grid (inc cx) 1) :status))
                  (recur grid (inc cx) 1 1 1 1 prev_collision)
                  (recur grid (inc cx) 1 1 1 1 prev_collision))
                (if (= :empty (get (g/cell grid cx (inc cy)) :status))
                  (recur grid cx (inc cy) 1 1 1 prev_collision)
                  (recur grid cx (inc cy) 1 1 1 prev_collision))))))))))
