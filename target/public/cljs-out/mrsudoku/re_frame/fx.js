// Compiled by ClojureScript 1.10.339 {}
goog.provide('re_frame.fx');
goog.require('cljs.core');
goog.require('re_frame.router');
goog.require('re_frame.db');
goog.require('re_frame.interceptor');
goog.require('re_frame.interop');
goog.require('re_frame.events');
goog.require('re_frame.registrar');
goog.require('re_frame.loggers');
goog.require('re_frame.trace');
re_frame.fx.kind = new cljs.core.Keyword(null,"fx","fx",-1237829572);
if(cljs.core.truth_(re_frame.registrar.kinds.call(null,re_frame.fx.kind))){
} else {
throw (new Error("Assert failed: (re-frame.registrar/kinds kind)"));
}
/**
 * Register the given effect `handler` for the given `id`.
 * 
 *   `id` is keyword, often namespaced.
 *   `handler` is a side-effecting function which takes a single argument and whose return
 *   value is ignored.
 * 
 *   Example Use
 *   -----------
 * 
 *   First, registration ... associate `:effect2` with a handler.
 * 
 *   (reg-fx
 *   :effect2
 *   (fn [value]
 *      ... do something side-effect-y))
 * 
 *   Then, later, if an event handler were to return this effects map ...
 * 
 *   {...
 * :effect2  [1 2]}
 * 
 * ... then the `handler` `fn` we registered previously, using `reg-fx`, will be
 * called with an argument of `[1 2]`.
 */
re_frame.fx.reg_fx = (function re_frame$fx$reg_fx(id,handler){
return re_frame.registrar.register_handler.call(null,re_frame.fx.kind,id,handler);
});
/**
 * An interceptor whose `:after` actions the contents of `:effects`. As a result,
 *   this interceptor is Domino 3.
 * 
 *   This interceptor is silently added (by reg-event-db etc) to the front of
 *   interceptor chains for all events.
 * 
 *   For each key in `:effects` (a map), it calls the registered `effects handler`
 *   (see `reg-fx` for registration of effect handlers).
 * 
 *   So, if `:effects` was:
 *    {:dispatch  [:hello 42]
 *     :db        {...}
 *     :undo      "set flag"}
 * 
 *   it will call the registered effect handlers for each of the map's keys:
 *   `:dispatch`, `:undo` and `:db`. When calling each handler, provides the map
 *   value for that key - so in the example above the effect handler for :dispatch
 *   will be given one arg `[:hello 42]`.
 * 
 *   You cannot rely on the ordering in which effects are executed.
 */
re_frame.fx.do_fx = re_frame.interceptor.__GT_interceptor.call(null,new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"do-fx","do-fx",1194163050),new cljs.core.Keyword(null,"after","after",594996914),(function re_frame$fx$do_fx_after(context){
if(re_frame.trace.is_trace_enabled_QMARK_.call(null)){
var _STAR_current_trace_STAR_11927 = re_frame.trace._STAR_current_trace_STAR_;
re_frame.trace._STAR_current_trace_STAR_ = re_frame.trace.start_trace.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword("event","do-fx","event/do-fx",1357330452)], null));

try{try{var seq__11928 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context));
var chunk__11929 = null;
var count__11930 = (0);
var i__11931 = (0);
while(true){
if((i__11931 < count__11930)){
var vec__11932 = cljs.core._nth.call(null,chunk__11929,i__11931);
var effect_key = cljs.core.nth.call(null,vec__11932,(0),null);
var effect_value = cljs.core.nth.call(null,vec__11932,(1),null);
var temp__5718__auto___11948 = re_frame.registrar.get_handler.call(null,re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5718__auto___11948)){
var effect_fn_11949 = temp__5718__auto___11948;
effect_fn_11949.call(null,effect_value);
} else {
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: no handler registered for effect:",effect_key,". Ignoring.");
}


var G__11950 = seq__11928;
var G__11951 = chunk__11929;
var G__11952 = count__11930;
var G__11953 = (i__11931 + (1));
seq__11928 = G__11950;
chunk__11929 = G__11951;
count__11930 = G__11952;
i__11931 = G__11953;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__11928);
if(temp__5720__auto__){
var seq__11928__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11928__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__11928__$1);
var G__11954 = cljs.core.chunk_rest.call(null,seq__11928__$1);
var G__11955 = c__4351__auto__;
var G__11956 = cljs.core.count.call(null,c__4351__auto__);
var G__11957 = (0);
seq__11928 = G__11954;
chunk__11929 = G__11955;
count__11930 = G__11956;
i__11931 = G__11957;
continue;
} else {
var vec__11935 = cljs.core.first.call(null,seq__11928__$1);
var effect_key = cljs.core.nth.call(null,vec__11935,(0),null);
var effect_value = cljs.core.nth.call(null,vec__11935,(1),null);
var temp__5718__auto___11958 = re_frame.registrar.get_handler.call(null,re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5718__auto___11958)){
var effect_fn_11959 = temp__5718__auto___11958;
effect_fn_11959.call(null,effect_value);
} else {
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: no handler registered for effect:",effect_key,". Ignoring.");
}


var G__11960 = cljs.core.next.call(null,seq__11928__$1);
var G__11961 = null;
var G__11962 = (0);
var G__11963 = (0);
seq__11928 = G__11960;
chunk__11929 = G__11961;
count__11930 = G__11962;
i__11931 = G__11963;
continue;
}
} else {
return null;
}
}
break;
}
}finally {if(re_frame.trace.is_trace_enabled_QMARK_.call(null)){
var end__11767__auto___11964 = re_frame.interop.now.call(null);
var duration__11768__auto___11965 = (end__11767__auto___11964 - new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_));
cljs.core.swap_BANG_.call(null,re_frame.trace.traces,cljs.core.conj,cljs.core.assoc.call(null,re_frame.trace._STAR_current_trace_STAR_,new cljs.core.Keyword(null,"duration","duration",1444101068),duration__11768__auto___11965,new cljs.core.Keyword(null,"end","end",-268185958),re_frame.interop.now.call(null)));

re_frame.trace.run_tracing_callbacks_BANG_.call(null,end__11767__auto___11964);
} else {
}
}}finally {re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR_11927;
}} else {
var seq__11938 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context));
var chunk__11939 = null;
var count__11940 = (0);
var i__11941 = (0);
while(true){
if((i__11941 < count__11940)){
var vec__11942 = cljs.core._nth.call(null,chunk__11939,i__11941);
var effect_key = cljs.core.nth.call(null,vec__11942,(0),null);
var effect_value = cljs.core.nth.call(null,vec__11942,(1),null);
var temp__5718__auto___11966 = re_frame.registrar.get_handler.call(null,re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5718__auto___11966)){
var effect_fn_11967 = temp__5718__auto___11966;
effect_fn_11967.call(null,effect_value);
} else {
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: no handler registered for effect:",effect_key,". Ignoring.");
}


var G__11968 = seq__11938;
var G__11969 = chunk__11939;
var G__11970 = count__11940;
var G__11971 = (i__11941 + (1));
seq__11938 = G__11968;
chunk__11939 = G__11969;
count__11940 = G__11970;
i__11941 = G__11971;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__11938);
if(temp__5720__auto__){
var seq__11938__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11938__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__11938__$1);
var G__11972 = cljs.core.chunk_rest.call(null,seq__11938__$1);
var G__11973 = c__4351__auto__;
var G__11974 = cljs.core.count.call(null,c__4351__auto__);
var G__11975 = (0);
seq__11938 = G__11972;
chunk__11939 = G__11973;
count__11940 = G__11974;
i__11941 = G__11975;
continue;
} else {
var vec__11945 = cljs.core.first.call(null,seq__11938__$1);
var effect_key = cljs.core.nth.call(null,vec__11945,(0),null);
var effect_value = cljs.core.nth.call(null,vec__11945,(1),null);
var temp__5718__auto___11976 = re_frame.registrar.get_handler.call(null,re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5718__auto___11976)){
var effect_fn_11977 = temp__5718__auto___11976;
effect_fn_11977.call(null,effect_value);
} else {
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: no handler registered for effect:",effect_key,". Ignoring.");
}


var G__11978 = cljs.core.next.call(null,seq__11938__$1);
var G__11979 = null;
var G__11980 = (0);
var G__11981 = (0);
seq__11938 = G__11978;
chunk__11939 = G__11979;
count__11940 = G__11980;
i__11941 = G__11981;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx.call(null,new cljs.core.Keyword(null,"dispatch-later","dispatch-later",291951390),(function (value){
var seq__11982 = cljs.core.seq.call(null,value);
var chunk__11983 = null;
var count__11984 = (0);
var i__11985 = (0);
while(true){
if((i__11985 < count__11984)){
var map__11986 = cljs.core._nth.call(null,chunk__11983,i__11985);
var map__11986__$1 = ((((!((map__11986 == null)))?(((((map__11986.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__11986.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__11986):map__11986);
var effect = map__11986__$1;
var ms = cljs.core.get.call(null,map__11986__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.call(null,map__11986__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_.call(null,dispatch)) || (!(typeof ms === 'number')))){
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch-later value:",effect);
} else {
re_frame.interop.set_timeout_BANG_.call(null,((function (seq__11982,chunk__11983,count__11984,i__11985,map__11986,map__11986__$1,effect,ms,dispatch){
return (function (){
return re_frame.router.dispatch.call(null,dispatch);
});})(seq__11982,chunk__11983,count__11984,i__11985,map__11986,map__11986__$1,effect,ms,dispatch))
,ms);
}


var G__11990 = seq__11982;
var G__11991 = chunk__11983;
var G__11992 = count__11984;
var G__11993 = (i__11985 + (1));
seq__11982 = G__11990;
chunk__11983 = G__11991;
count__11984 = G__11992;
i__11985 = G__11993;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__11982);
if(temp__5720__auto__){
var seq__11982__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11982__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__11982__$1);
var G__11994 = cljs.core.chunk_rest.call(null,seq__11982__$1);
var G__11995 = c__4351__auto__;
var G__11996 = cljs.core.count.call(null,c__4351__auto__);
var G__11997 = (0);
seq__11982 = G__11994;
chunk__11983 = G__11995;
count__11984 = G__11996;
i__11985 = G__11997;
continue;
} else {
var map__11988 = cljs.core.first.call(null,seq__11982__$1);
var map__11988__$1 = ((((!((map__11988 == null)))?(((((map__11988.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__11988.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__11988):map__11988);
var effect = map__11988__$1;
var ms = cljs.core.get.call(null,map__11988__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.call(null,map__11988__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_.call(null,dispatch)) || (!(typeof ms === 'number')))){
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch-later value:",effect);
} else {
re_frame.interop.set_timeout_BANG_.call(null,((function (seq__11982,chunk__11983,count__11984,i__11985,map__11988,map__11988__$1,effect,ms,dispatch,seq__11982__$1,temp__5720__auto__){
return (function (){
return re_frame.router.dispatch.call(null,dispatch);
});})(seq__11982,chunk__11983,count__11984,i__11985,map__11988,map__11988__$1,effect,ms,dispatch,seq__11982__$1,temp__5720__auto__))
,ms);
}


var G__11998 = cljs.core.next.call(null,seq__11982__$1);
var G__11999 = null;
var G__12000 = (0);
var G__12001 = (0);
seq__11982 = G__11998;
chunk__11983 = G__11999;
count__11984 = G__12000;
i__11985 = G__12001;
continue;
}
} else {
return null;
}
}
break;
}
}));
re_frame.fx.reg_fx.call(null,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),(function (value){
if(!(cljs.core.vector_QMARK_.call(null,value))){
return re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch value. Expected a vector, but got:",value);
} else {
return re_frame.router.dispatch.call(null,value);
}
}));
re_frame.fx.reg_fx.call(null,new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),(function (value){
if(!(cljs.core.sequential_QMARK_.call(null,value))){
return re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch-n value. Expected a collection, got got:",value);
} else {
var seq__12002 = cljs.core.seq.call(null,cljs.core.remove.call(null,cljs.core.nil_QMARK_,value));
var chunk__12003 = null;
var count__12004 = (0);
var i__12005 = (0);
while(true){
if((i__12005 < count__12004)){
var event = cljs.core._nth.call(null,chunk__12003,i__12005);
re_frame.router.dispatch.call(null,event);


var G__12006 = seq__12002;
var G__12007 = chunk__12003;
var G__12008 = count__12004;
var G__12009 = (i__12005 + (1));
seq__12002 = G__12006;
chunk__12003 = G__12007;
count__12004 = G__12008;
i__12005 = G__12009;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__12002);
if(temp__5720__auto__){
var seq__12002__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__12002__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__12002__$1);
var G__12010 = cljs.core.chunk_rest.call(null,seq__12002__$1);
var G__12011 = c__4351__auto__;
var G__12012 = cljs.core.count.call(null,c__4351__auto__);
var G__12013 = (0);
seq__12002 = G__12010;
chunk__12003 = G__12011;
count__12004 = G__12012;
i__12005 = G__12013;
continue;
} else {
var event = cljs.core.first.call(null,seq__12002__$1);
re_frame.router.dispatch.call(null,event);


var G__12014 = cljs.core.next.call(null,seq__12002__$1);
var G__12015 = null;
var G__12016 = (0);
var G__12017 = (0);
seq__12002 = G__12014;
chunk__12003 = G__12015;
count__12004 = G__12016;
i__12005 = G__12017;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx.call(null,new cljs.core.Keyword(null,"deregister-event-handler","deregister-event-handler",-1096518994),(function (value){
var clear_event = cljs.core.partial.call(null,re_frame.registrar.clear_handlers,re_frame.events.kind);
if(cljs.core.sequential_QMARK_.call(null,value)){
var seq__12018 = cljs.core.seq.call(null,value);
var chunk__12019 = null;
var count__12020 = (0);
var i__12021 = (0);
while(true){
if((i__12021 < count__12020)){
var event = cljs.core._nth.call(null,chunk__12019,i__12021);
clear_event.call(null,event);


var G__12022 = seq__12018;
var G__12023 = chunk__12019;
var G__12024 = count__12020;
var G__12025 = (i__12021 + (1));
seq__12018 = G__12022;
chunk__12019 = G__12023;
count__12020 = G__12024;
i__12021 = G__12025;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__12018);
if(temp__5720__auto__){
var seq__12018__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__12018__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__12018__$1);
var G__12026 = cljs.core.chunk_rest.call(null,seq__12018__$1);
var G__12027 = c__4351__auto__;
var G__12028 = cljs.core.count.call(null,c__4351__auto__);
var G__12029 = (0);
seq__12018 = G__12026;
chunk__12019 = G__12027;
count__12020 = G__12028;
i__12021 = G__12029;
continue;
} else {
var event = cljs.core.first.call(null,seq__12018__$1);
clear_event.call(null,event);


var G__12030 = cljs.core.next.call(null,seq__12018__$1);
var G__12031 = null;
var G__12032 = (0);
var G__12033 = (0);
seq__12018 = G__12030;
chunk__12019 = G__12031;
count__12020 = G__12032;
i__12021 = G__12033;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return clear_event.call(null,value);
}
}));
re_frame.fx.reg_fx.call(null,new cljs.core.Keyword(null,"db","db",993250759),(function (value){
if(!((cljs.core.deref.call(null,re_frame.db.app_db) === value))){
return cljs.core.reset_BANG_.call(null,re_frame.db.app_db,value);
} else {
return null;
}
}));

//# sourceMappingURL=fx.js.map
