// Compiled by ClojureScript 1.10.339 {}
goog.provide('re_frame.trace');
goog.require('cljs.core');
goog.require('re_frame.interop');
goog.require('re_frame.loggers');
goog.require('goog.functions');
re_frame.trace.id = cljs.core.atom.call(null,(0));
re_frame.trace._STAR_current_trace_STAR_ = null;
re_frame.trace.reset_tracing_BANG_ = (function re_frame$trace$reset_tracing_BANG_(){
return cljs.core.reset_BANG_.call(null,re_frame.trace.id,(0));
});

/** @define {boolean} */
goog.define("re_frame.trace.trace_enabled_QMARK_",false);
/**
 * See https://groups.google.com/d/msg/clojurescript/jk43kmYiMhA/IHglVr_TPdgJ for more details
 */
re_frame.trace.is_trace_enabled_QMARK_ = (function re_frame$trace$is_trace_enabled_QMARK_(){
return re_frame.trace.trace_enabled_QMARK_;
});
re_frame.trace.trace_cbs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
if((typeof re_frame !== 'undefined') && (typeof re_frame.trace !== 'undefined') && (typeof re_frame.trace.traces !== 'undefined')){
} else {
re_frame.trace.traces = cljs.core.atom.call(null,cljs.core.PersistentVector.EMPTY);
}
if((typeof re_frame !== 'undefined') && (typeof re_frame.trace !== 'undefined') && (typeof re_frame.trace.next_delivery !== 'undefined')){
} else {
re_frame.trace.next_delivery = cljs.core.atom.call(null,(0));
}
/**
 * Registers a tracing callback function which will receive a collection of one or more traces.
 *   Will replace an existing callback function if it shares the same key.
 */
re_frame.trace.register_trace_cb = (function re_frame$trace$register_trace_cb(key,f){
if(re_frame.trace.trace_enabled_QMARK_){
return cljs.core.swap_BANG_.call(null,re_frame.trace.trace_cbs,cljs.core.assoc,key,f);
} else {
return re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),"Tracing is not enabled. Please set {\"re_frame.trace.trace_enabled_QMARK_\" true} in :closure-defines. See: https://github.com/Day8/re-frame-trace#installation.");
}
});
re_frame.trace.remove_trace_cb = (function re_frame$trace$remove_trace_cb(key){
cljs.core.swap_BANG_.call(null,re_frame.trace.trace_cbs,cljs.core.dissoc,key);

return null;
});
re_frame.trace.next_id = (function re_frame$trace$next_id(){
return cljs.core.swap_BANG_.call(null,re_frame.trace.id,cljs.core.inc);
});
re_frame.trace.start_trace = (function re_frame$trace$start_trace(p__11789){
var map__11790 = p__11789;
var map__11790__$1 = ((((!((map__11790 == null)))?(((((map__11790.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__11790.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__11790):map__11790);
var operation = cljs.core.get.call(null,map__11790__$1,new cljs.core.Keyword(null,"operation","operation",-1267664310));
var op_type = cljs.core.get.call(null,map__11790__$1,new cljs.core.Keyword(null,"op-type","op-type",-1636141668));
var tags = cljs.core.get.call(null,map__11790__$1,new cljs.core.Keyword(null,"tags","tags",1771418977));
var child_of = cljs.core.get.call(null,map__11790__$1,new cljs.core.Keyword(null,"child-of","child-of",-903376662));
return new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"id","id",-1388402092),re_frame.trace.next_id.call(null),new cljs.core.Keyword(null,"operation","operation",-1267664310),operation,new cljs.core.Keyword(null,"op-type","op-type",-1636141668),op_type,new cljs.core.Keyword(null,"tags","tags",1771418977),tags,new cljs.core.Keyword(null,"child-of","child-of",-903376662),(function (){var or__3949__auto__ = child_of;
if(cljs.core.truth_(or__3949__auto__)){
return or__3949__auto__;
} else {
return new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_);
}
})(),new cljs.core.Keyword(null,"start","start",-355208981),re_frame.interop.now.call(null)], null);
});
re_frame.trace.debounce_time = (50);
re_frame.trace.debounce = (function re_frame$trace$debounce(f,interval){
return goog.functions.debounce(f,interval);
});
re_frame.trace.schedule_debounce = re_frame.trace.debounce.call(null,(function re_frame$trace$tracing_cb_debounced(){
var seq__11792_11804 = cljs.core.seq.call(null,cljs.core.deref.call(null,re_frame.trace.trace_cbs));
var chunk__11793_11805 = null;
var count__11794_11806 = (0);
var i__11795_11807 = (0);
while(true){
if((i__11795_11807 < count__11794_11806)){
var vec__11796_11808 = cljs.core._nth.call(null,chunk__11793_11805,i__11795_11807);
var k_11809 = cljs.core.nth.call(null,vec__11796_11808,(0),null);
var cb_11810 = cljs.core.nth.call(null,vec__11796_11808,(1),null);
try{cb_11810.call(null,cljs.core.deref.call(null,re_frame.trace.traces));
}catch (e11799){var e_11811 = e11799;
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"Error thrown from trace cb",k_11809,"while storing",cljs.core.deref.call(null,re_frame.trace.traces),e_11811);
}

var G__11812 = seq__11792_11804;
var G__11813 = chunk__11793_11805;
var G__11814 = count__11794_11806;
var G__11815 = (i__11795_11807 + (1));
seq__11792_11804 = G__11812;
chunk__11793_11805 = G__11813;
count__11794_11806 = G__11814;
i__11795_11807 = G__11815;
continue;
} else {
var temp__5720__auto___11816 = cljs.core.seq.call(null,seq__11792_11804);
if(temp__5720__auto___11816){
var seq__11792_11817__$1 = temp__5720__auto___11816;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11792_11817__$1)){
var c__4351__auto___11818 = cljs.core.chunk_first.call(null,seq__11792_11817__$1);
var G__11819 = cljs.core.chunk_rest.call(null,seq__11792_11817__$1);
var G__11820 = c__4351__auto___11818;
var G__11821 = cljs.core.count.call(null,c__4351__auto___11818);
var G__11822 = (0);
seq__11792_11804 = G__11819;
chunk__11793_11805 = G__11820;
count__11794_11806 = G__11821;
i__11795_11807 = G__11822;
continue;
} else {
var vec__11800_11823 = cljs.core.first.call(null,seq__11792_11817__$1);
var k_11824 = cljs.core.nth.call(null,vec__11800_11823,(0),null);
var cb_11825 = cljs.core.nth.call(null,vec__11800_11823,(1),null);
try{cb_11825.call(null,cljs.core.deref.call(null,re_frame.trace.traces));
}catch (e11803){var e_11826 = e11803;
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"Error thrown from trace cb",k_11824,"while storing",cljs.core.deref.call(null,re_frame.trace.traces),e_11826);
}

var G__11827 = cljs.core.next.call(null,seq__11792_11817__$1);
var G__11828 = null;
var G__11829 = (0);
var G__11830 = (0);
seq__11792_11804 = G__11827;
chunk__11793_11805 = G__11828;
count__11794_11806 = G__11829;
i__11795_11807 = G__11830;
continue;
}
} else {
}
}
break;
}

return cljs.core.reset_BANG_.call(null,re_frame.trace.traces,cljs.core.PersistentVector.EMPTY);
}),re_frame.trace.debounce_time);
re_frame.trace.run_tracing_callbacks_BANG_ = (function re_frame$trace$run_tracing_callbacks_BANG_(now){
if(((cljs.core.deref.call(null,re_frame.trace.next_delivery) - (10)) < now)){
re_frame.trace.schedule_debounce.call(null);

return cljs.core.reset_BANG_.call(null,re_frame.trace.next_delivery,(now + re_frame.trace.debounce_time));
} else {
return null;
}
});

//# sourceMappingURL=trace.js.map
