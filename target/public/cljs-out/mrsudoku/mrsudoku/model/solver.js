// Compiled by ClojureScript 1.10.339 {}
goog.provide('mrsudoku.model.solver');
goog.require('cljs.core');
goog.require('mrsudoku.model.grid');
goog.require('mrsudoku.model.conflict');
/**
 * Solve the sudoku `grid` by returing a full solved grid,
 *  or `nil` if the solver fails.
 */
mrsudoku.model.solver.solve = (function mrsudoku$model$solver$solve(grid){
var grid__$1 = grid;
var cx = (1);
var cy = (1);
var val = (1);
var count = (1);
var rev = (1);
var prev_collision = (100);
while(true){
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"init","init",-1875481434),cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$1,cx,cy),new cljs.core.Keyword(null,"status","status",-1997798413)))){
if(cljs.core._EQ_.call(null,rev,(1))){
if(((cljs.core._EQ_.call(null,cx,(9))) && (cljs.core._EQ_.call(null,cy,(9))))){
return grid__$1;
} else {
if(cljs.core._EQ_.call(null,cx,(10))){
return null;
} else {
if(cljs.core._EQ_.call(null,cy,(9))){
var G__8542 = grid__$1;
var G__8543 = (cx + (1));
var G__8544 = (1);
var G__8545 = (1);
var G__8546 = (1);
var G__8547 = rev;
var G__8548 = prev_collision;
grid__$1 = G__8542;
cx = G__8543;
cy = G__8544;
val = G__8545;
count = G__8546;
rev = G__8547;
prev_collision = G__8548;
continue;
} else {
var G__8549 = grid__$1;
var G__8550 = cx;
var G__8551 = (cy + (1));
var G__8552 = (1);
var G__8553 = (1);
var G__8554 = rev;
var G__8555 = prev_collision;
grid__$1 = G__8549;
cx = G__8550;
cy = G__8551;
val = G__8552;
count = G__8553;
rev = G__8554;
prev_collision = G__8555;
continue;
}
}
}
} else {
if(cljs.core._EQ_.call(null,cy,(1))){
if(cljs.core._EQ_.call(null,cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$1,(cx - (1)),(9)),new cljs.core.Keyword(null,"value","value",305978217)),(9))){
var G__8556 = grid__$1;
var G__8557 = (cx - (1));
var G__8558 = (9);
var G__8559 = (cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$1,(cx - (1)),(9)),new cljs.core.Keyword(null,"value","value",305978217)) + (1));
var G__8560 = (0);
var G__8561 = rev;
var G__8562 = prev_collision;
grid__$1 = G__8556;
cx = G__8557;
cy = G__8558;
val = G__8559;
count = G__8560;
rev = G__8561;
prev_collision = G__8562;
continue;
} else {
var G__8563 = grid__$1;
var G__8564 = (cx - (1));
var G__8565 = (9);
var G__8566 = (cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$1,(cx - (1)),(9)),new cljs.core.Keyword(null,"value","value",305978217)) + (1));
var G__8567 = (1);
var G__8568 = rev;
var G__8569 = prev_collision;
grid__$1 = G__8563;
cx = G__8564;
cy = G__8565;
val = G__8566;
count = G__8567;
rev = G__8568;
prev_collision = G__8569;
continue;
}
} else {
if(cljs.core._EQ_.call(null,cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$1,cx,(cy - (1))),new cljs.core.Keyword(null,"value","value",305978217)),(9))){
var G__8570 = grid__$1;
var G__8571 = cx;
var G__8572 = (cy - (1));
var G__8573 = (cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$1,cx,(cy - (1))),new cljs.core.Keyword(null,"value","value",305978217)) + (1));
var G__8574 = (0);
var G__8575 = rev;
var G__8576 = prev_collision;
grid__$1 = G__8570;
cx = G__8571;
cy = G__8572;
val = G__8573;
count = G__8574;
rev = G__8575;
prev_collision = G__8576;
continue;
} else {
var G__8577 = grid__$1;
var G__8578 = cx;
var G__8579 = (cy - (1));
var G__8580 = (cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$1,cx,(cy - (1))),new cljs.core.Keyword(null,"value","value",305978217)) + (1));
var G__8581 = (1);
var G__8582 = rev;
var G__8583 = prev_collision;
grid__$1 = G__8577;
cx = G__8578;
cy = G__8579;
val = G__8580;
count = G__8581;
rev = G__8582;
prev_collision = G__8583;
continue;
}
}
}
} else {
var grid__$2 = mrsudoku.model.grid.change_cell.call(null,grid__$1,cx,cy,mrsudoku.model.grid.mk_cell.call(null,new cljs.core.Keyword(null,"set","set",304602554),val));
cljs.core.println.call(null,"conflicts = ",mrsudoku.model.conflict.grid_conflicts.call(null,grid__$2));

if(((cljs.core._EQ_.call(null,count,(0))) || (cljs.core.not_EQ_.call(null,mrsudoku.model.conflict.grid_conflicts.call(null,grid__$2),cljs.core.PersistentArrayMap.EMPTY)))){
if((val >= (9))){
if(cljs.core._EQ_.call(null,cy,(1))){
if(cljs.core._EQ_.call(null,cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$2,(cx - (1)),(9)),new cljs.core.Keyword(null,"value","value",305978217)),(9))){
var G__8584 = mrsudoku.model.grid.change_cell.call(null,grid__$2,cx,cy,mrsudoku.model.grid.mk_cell.call(null,new cljs.core.Keyword(null,"set","set",304602554),prev_collision));
var G__8585 = (cx - (1));
var G__8586 = (9);
var G__8587 = (cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$2,(cx - (1)),(9)),new cljs.core.Keyword(null,"value","value",305978217)) + (1));
var G__8588 = (0);
var G__8589 = (-1);
var G__8590 = (prev_collision + (1));
grid__$1 = G__8584;
cx = G__8585;
cy = G__8586;
val = G__8587;
count = G__8588;
rev = G__8589;
prev_collision = G__8590;
continue;
} else {
var G__8591 = mrsudoku.model.grid.change_cell.call(null,grid__$2,cx,cy,mrsudoku.model.grid.mk_cell.call(null,new cljs.core.Keyword(null,"set","set",304602554),prev_collision));
var G__8592 = (cx - (1));
var G__8593 = (9);
var G__8594 = (cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$2,(cx - (1)),(9)),new cljs.core.Keyword(null,"value","value",305978217)) + (1));
var G__8595 = (1);
var G__8596 = (-1);
var G__8597 = (prev_collision + (1));
grid__$1 = G__8591;
cx = G__8592;
cy = G__8593;
val = G__8594;
count = G__8595;
rev = G__8596;
prev_collision = G__8597;
continue;
}
} else {
if(cljs.core._EQ_.call(null,cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$2,cx,(cy - (1))),new cljs.core.Keyword(null,"value","value",305978217)),(9))){
var G__8598 = mrsudoku.model.grid.change_cell.call(null,grid__$2,cx,cy,mrsudoku.model.grid.mk_cell.call(null,new cljs.core.Keyword(null,"set","set",304602554),prev_collision));
var G__8599 = cx;
var G__8600 = (cy - (1));
var G__8601 = (cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$2,cx,(cy - (1))),new cljs.core.Keyword(null,"value","value",305978217)) + (1));
var G__8602 = (0);
var G__8603 = (-1);
var G__8604 = (prev_collision + (1));
grid__$1 = G__8598;
cx = G__8599;
cy = G__8600;
val = G__8601;
count = G__8602;
rev = G__8603;
prev_collision = G__8604;
continue;
} else {
var G__8605 = mrsudoku.model.grid.change_cell.call(null,grid__$2,cx,cy,mrsudoku.model.grid.mk_cell.call(null,new cljs.core.Keyword(null,"set","set",304602554),prev_collision));
var G__8606 = cx;
var G__8607 = (cy - (1));
var G__8608 = (cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$2,cx,(cy - (1))),new cljs.core.Keyword(null,"value","value",305978217)) + (1));
var G__8609 = (1);
var G__8610 = (-1);
var G__8611 = (prev_collision + (1));
grid__$1 = G__8605;
cx = G__8606;
cy = G__8607;
val = G__8608;
count = G__8609;
rev = G__8610;
prev_collision = G__8611;
continue;
}
}
} else {
var G__8612 = grid__$2;
var G__8613 = cx;
var G__8614 = cy;
var G__8615 = (val + (1));
var G__8616 = (1);
var G__8617 = (1);
var G__8618 = prev_collision;
grid__$1 = G__8612;
cx = G__8613;
cy = G__8614;
val = G__8615;
count = G__8616;
rev = G__8617;
prev_collision = G__8618;
continue;
}
} else {
if(((cljs.core._EQ_.call(null,cx,(9))) && (cljs.core._EQ_.call(null,cy,(9))))){
return grid__$2;
} else {
if(cljs.core._EQ_.call(null,cx,(10))){
return null;
} else {
if(cljs.core._EQ_.call(null,cy,(9))){
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"empty","empty",767870958),cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$2,(cx + (1)),(1)),new cljs.core.Keyword(null,"status","status",-1997798413)))){
var G__8619 = grid__$2;
var G__8620 = (cx + (1));
var G__8621 = (1);
var G__8622 = (1);
var G__8623 = (1);
var G__8624 = (1);
var G__8625 = prev_collision;
grid__$1 = G__8619;
cx = G__8620;
cy = G__8621;
val = G__8622;
count = G__8623;
rev = G__8624;
prev_collision = G__8625;
continue;
} else {
var G__8626 = grid__$2;
var G__8627 = (cx + (1));
var G__8628 = (1);
var G__8629 = (1);
var G__8630 = (1);
var G__8631 = (1);
var G__8632 = prev_collision;
grid__$1 = G__8626;
cx = G__8627;
cy = G__8628;
val = G__8629;
count = G__8630;
rev = G__8631;
prev_collision = G__8632;
continue;
}
} else {
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"empty","empty",767870958),cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid__$2,cx,(cy + (1))),new cljs.core.Keyword(null,"status","status",-1997798413)))){
var G__8633 = grid__$2;
var G__8634 = cx;
var G__8635 = (cy + (1));
var G__8636 = (1);
var G__8637 = (1);
var G__8638 = (1);
var G__8639 = prev_collision;
grid__$1 = G__8633;
cx = G__8634;
cy = G__8635;
val = G__8636;
count = G__8637;
rev = G__8638;
prev_collision = G__8639;
continue;
} else {
var G__8640 = grid__$2;
var G__8641 = cx;
var G__8642 = (cy + (1));
var G__8643 = (1);
var G__8644 = (1);
var G__8645 = (1);
var G__8646 = prev_collision;
grid__$1 = G__8640;
cx = G__8641;
cy = G__8642;
val = G__8643;
count = G__8644;
rev = G__8645;
prev_collision = G__8646;
continue;
}
}
}
}
}
}
break;
}
});

//# sourceMappingURL=solver.js.map
